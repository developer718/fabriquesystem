from email import message
from django.test import TestCase

from api.models import Mailing, Client, Message

class MailingModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Mailing.objects.create(start='2023-05-24T11:00:00Z', text='For test', phoneTag='7', label='Fr', end='2023-06-24T11:00:00Z')


    def test_text_max_length(self):
        mailing = Mailing.objects.get(id = 1)
        max_length = mailing._meta.get_field('text').max_length
        self.assertEquals(max_length, 511)

    
    def test_phoneTag_max_length(self):
        mailing = Mailing.objects.get(id = 1)
        max_length = mailing._meta.get_field('phoneTag').max_length
        self.assertEquals(max_length, 1)

    
    def test_label_max_length(self):
        mailing = Mailing.objects.get(id = 1)
        max_length = mailing._meta.get_field('label').max_length
        self.assertEquals(max_length, 7)

    
    def test_str_method(self):
        mailing = Mailing.objects.get(id = 1)
        text = mailing.text
        returnedText = mailing.__str__()
        self.assertEquals(text, returnedText)


class ClientModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Client.objects.create(phoneNumber='79153334445', mobileOperatorCode='915', label='Fr', timezone='UTC+5')


    def test_phoneNumber_max_length(self):
        client = Client.objects.get(id = 1)
        max_length = client._meta.get_field('phoneNumber').max_length
        self.assertEquals(max_length, 11)

    
    def test_label_max_length(self):
        client = Client.objects.get(id = 1)
        max_length = client._meta.get_field('label').max_length
        self.assertEquals(max_length, 7)

    
    def test_timezone_max_length(self):
        client = Client.objects.get(id = 1)
        max_length = client._meta.get_field('timezone').max_length
        self.assertEquals(max_length, 6)

    
    def test_getMobileOperatorCode_method(self):
        client = Client.objects.get(id = 1)
        code = client.mobileOperatorCode
        returnedCode = client.getMobileOperatorCode()
        self.assertEquals(int(code), int(returnedCode))


    def test_str_method(self):
        client = Client.objects.get(id = 1)
        text = "1 79153334445"
        returnedText = client.__str__()
        self.assertEquals(text, returnedText)


class MessageModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        mailing = Mailing.objects.create(start='2023-05-24T11:00:00Z', text='For test', phoneTag='7', label='Fr', end='2023-06-24T11:00:00Z')
        client = Client.objects.create(phoneNumber='79153334445', mobileOperatorCode='915', label='Fr', timezone='UTC+5')
        Message.objects.create(wasSent = '2023-05-24T11:00:00Z', status = True, mailing = mailing, client = client)

    
    def test_str_method(self):
        message = Message.objects.get(id = 1)
        text = "1 2 2"
        returnedText = message.__str__()
        self.assertEquals(text, returnedText)