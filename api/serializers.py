from rest_framework import serializers

from .models import Mailing, Client, Message

class MailingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'start', 'text', 'phoneTag', 'label', 'end')

    
class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ('id', "phoneNumber", "mobileOperatorCode", "label", "timezone")


class MessageSerializer(serializers.HyperlinkedModelSerializer):  
    class Meta:
        model = Message
        fields = ('id', "wasSent", "status", "mailing", "client")