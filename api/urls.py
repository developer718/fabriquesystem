from django.urls import include, path
from rest_framework import routers
from . import views

from fabriquesystem.yasg import urlpatterns as doc_urls


router = routers.DefaultRouter()
router.register(r'api/v1/messages', views.MessageViewSet)


urlpatterns = [
    path('', include(router.urls)),

    path('api/v1/mailings/', views.MailingAPIList.as_view()),
    path('api/v1/mailing-update/<int:pk>/', views.MailingAPIUpdate.as_view(), name='mailing-detail'),
    path('api/v1/mailing-delete/<int:pk>/', views.MailingAPIDestroy.as_view()),
    path('api/v1/mailings/info/', views.MailingInfo.as_view()),
    path('api/v1/mailings/available-mailings/', views.AvailableMailingAPI.as_view()),
    path('api/v1/mailings/future-mailings/', views.FutureMailings.as_view()),
    path('api/v1/mailings/available-mailings/start/', views.StartMailing.as_view()),

    path('api/v1/clients/', views.ClientAPIList.as_view()),
    path('api/v1/client-update/<int:pk>/', views.ClientAPIUpdate.as_view(), name='client-detail'),
    path('api/v1/client-delete/<int:pk>/', views.ClientAPIDestroy.as_view()),

    path('api/v1/messages-with-mailing/<int:mailing>', views.MessageList.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += doc_urls