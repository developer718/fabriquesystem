from django.db import models
from django.core.validators import RegexValidator
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver


class Mailing(models.Model):
    start = models.DateTimeField()
    text = models.TextField(max_length=511)
    phoneTag = models.CharField(max_length=1)
    label = models.CharField(max_length=7)
    end = models.DateTimeField()

    def __str__(self):

        return self.text


TIMEZONE_CHOICES = (
    ("UTM+2", "UTM+2"),
    ("UTM+3", "UTM+3"),
    ("UTM+4", "UTM+4"),
    ("UTM+5", "UTM+5"),
    ("UTM+6", "UTM+6"),
    ("UTM+7", "UTM+7"),
    ("UTM+8", "UTM+8"),
    ("UTM+9", "UTM+9"),
    ("UTM+10", "UTM+10"),
    ("UTM+11", "UTM+11"),
    ("UTM+12", "UTM+12"),
)
class Client(models.Model):
    phoneRegex = RegexValidator(regex = r'^7[0-9]{10}$')
    phoneNumber = models.CharField(validators=[phoneRegex], max_length=11)
    mobileOperatorCode = models.PositiveSmallIntegerField(default=1)
    label = models.CharField(max_length=7)
    timezone = models.CharField(choices = TIMEZONE_CHOICES, max_length=6)

    def getMobileOperatorCode(self):
        return self.phoneNumber[1:4]

 
    def __str__(self) -> str:
        return str(self.id) + " " + self.phoneNumber

@receiver(pre_save, sender=Client)
def setMobileOperatorCode(sender, instance, **kwargs):
    instance.mobileOperatorCode = instance.getMobileOperatorCode()


STATUS_CHOICES = (
    (True, True),
    (False, False)
)
class Message(models.Model):
    wasSent = models.DateTimeField()
    status = models.BooleanField(choices=STATUS_CHOICES)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("mailing", "client")

    def __str__(self):
        return  str(self.id) + " " + str(self.client.id) + " " + str(self.mailing.id)
