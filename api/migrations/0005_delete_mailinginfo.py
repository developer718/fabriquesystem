# Generated by Django 4.0.4 on 2022-04-24 15:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_mailinginfo'),
    ]

    operations = [
        migrations.DeleteModel(
            name='MailingInfo',
        ),
    ]
