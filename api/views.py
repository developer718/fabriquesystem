from django.shortcuts import render

import numpy as np
from datetime import datetime, timedelta, timezone
from psycopg2 import IntegrityError
import pytz
import requests
import json

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny, IsAdminUser
from rest_framework.views import APIView

from .serializers import MailingSerializer, ClientSerializer, MessageSerializer
from .models import Mailing, Client, Message


class MailingAPIList(generics.ListCreateAPIView): # all mailings
    queryset = Mailing.objects.all().order_by('-start')
    serializer_class = MailingSerializer
    permission_classes = [AllowAny]


class MailingAPIUpdate(generics.RetrieveUpdateAPIView): # update one mailing
    queryset = Mailing.objects.all().order_by('-start')
    serializer_class = MailingSerializer
    permission_classes = [IsAdminUser]


class MailingAPIDestroy(generics.RetrieveDestroyAPIView): # delete one mailing
    queryset = Mailing.objects.all().order_by('-start')
    serializer_class = MailingSerializer
    permission_classes = [IsAdminUser]


class ClientAPIList(generics.ListCreateAPIView): # all clients
    queryset = Client.objects.all().order_by('id')
    serializer_class = ClientSerializer
    permission_classes = [AllowAny]


class ClientAPIUpdate(generics.RetrieveUpdateAPIView): # update one client
    queryset = Client.objects.all().order_by('id')
    serializer_class = ClientSerializer
    permission_classes = [IsAdminUser]


class ClientAPIDestroy(generics.RetrieveDestroyAPIView): # delete one client
    queryset = Client.objects.all().order_by('id')
    serializer_class = ClientSerializer
    permission_classes = [IsAdminUser]


class MessageList(generics.ListAPIView): # all messages in one mailing
    serializer_class = MessageSerializer

    def get_queryset(self):
        mailing = self.kwargs['mailing']
        return Message.objects.filter(mailing=mailing).order_by('-status')


class MessageViewSet(viewsets.ModelViewSet): # method for all Message objects
    queryset = Message.objects.all().order_by('-wasSent')
    serializer_class = MessageSerializer


class AvailableMailingAPI(generics.ListAPIView):
    serializer_class = MailingSerializer
    def get_queryset(self):

        mailingsList = list(Mailing.objects.all())
        
        for mailing in mailingsList:
            if not ((datetime.now(timezone.utc) - mailing.start).seconds >= 0 and (datetime.now(timezone.utc)-mailing.end).seconds <= 0):
                mailingsList.remove(mailing)
        return mailingsList


class StartMailing(generics.ListAPIView):
    serializer_class = MessageSerializer
    permission_classes = [IsAdminUser]

    def get_queryset(self):

        mailingsList = list(Mailing.objects.all())

        clientsList = list(Client.objects.all())
        
        for mailing in mailingsList:
            if not ((datetime.now(timezone.utc) + timedelta(hours=3)- mailing.start).seconds >= 0 
            and (datetime.now(timezone.utc) + timedelta(hours=3) - mailing.end).seconds <= 0):
                mailingsList.remove(mailing)
        
        for mailing in mailingsList:
            for client in clientsList:
                if client.phoneNumber[0] == mailing.phoneTag and client.label == mailing.label:
                    try:

                        message = Message.objects.create(wasSent = datetime.now(timezone.utc) + timedelta(hours=3), status = True, mailing = mailing, 
                        client = client)

                        url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
                        params = {"id": message.id, "phone": client.phoneNumber, "text": mailing.text}
                        headers = {'accept': 'application/json',
                                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODE5OTExNzIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlZsYWQifQ.B4Jhd7Ar0PnJPuE9X_ux_z9r6Xl3UU91j8tbUpUI7Ck',
                                'Content-Type': 'application/json'}

                        res = requests.post(url, headers = headers, json = params)

                        if res.status_code == 200:
                            continue
                        else:
                            message.status = False
                    except:
                        continue
                
        queryset = Message.objects.all().order_by('-wasSent')
        return queryset



class MailingInfo(generics.ListAPIView):
    serializer_class = MailingSerializer

    def get_queryset(self):
        mailings = Mailing.objects.all().order_by('-start')
        return mailings

    def list(self, request, *args, **kwargs):
        mailings = super(MailingInfo, self).list(request, *args, **kwargs)
        messages = Message.objects.all()
        res = {"mailing": mailings.data, "messages_sent": len(messages)}
        return Response(res)


class FutureMailings(generics.ListAPIView):
    serializer_class = MailingSerializer

    def get_queryset(self):
        mailingsList = list(Mailing.objects.all().order_by('-start'))

        futureMailings = []

        for mailing in mailingsList:
            if datetime.now(timezone.utc) + timedelta(hours=3) < mailing.start:
                futureMailings.append(mailing)

        return futureMailings
