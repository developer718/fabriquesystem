Task for Fabrique Company.

This project is API for managing clients of company, its mailings and checking messages. I used API service https://probe.fbrq.cloud/docs for "sending" message to client for each specific mailing.

I also completed additional tasks: 
    1. code testing
    5. Swagger UI on /docs/, OpenAPI on /redoc/
    6. realization of Admin Web UI
